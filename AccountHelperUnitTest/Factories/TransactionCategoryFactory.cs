﻿using AccountHelper.Classes;
using AccountHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountHelperUnitTest.Factories
{
    public enum CategoryIdEnum
    {
        GLOBAL,
        FOOD,
        RENT,
        VEGETABLE,
        MEAT
    }

    static public class TransactionCategoryFactory
    {
        static public List<TransactionCategory> getTransactionCategoriesValidTree()
        {
            List<TransactionCategory> outValue = new List<AccountHelper.TransactionCategory>();
            outValue.Add(createNewCategory(CategoryIdEnum.GLOBAL, CategoryIdEnum.GLOBAL));
            outValue.Add(createNewCategory(CategoryIdEnum.FOOD, CategoryIdEnum.GLOBAL));
            outValue.Add(createNewCategory(CategoryIdEnum.RENT, CategoryIdEnum.GLOBAL));
            outValue.Add(createNewCategory(CategoryIdEnum.VEGETABLE, CategoryIdEnum.FOOD));
            outValue.Add(createNewCategory(CategoryIdEnum.MEAT, CategoryIdEnum.FOOD));

            return outValue;
        }

        /// <summary>
        ///  Just gives a wrongly formatted tree structure
        /// </summary>
        static public List<TransactionCategory> getTransactionCategoriesInvalidTree()
        {
            List<TransactionCategory> outValue = new List<AccountHelper.TransactionCategory>();
            outValue.Add(createNewCategory(CategoryIdEnum.GLOBAL, CategoryIdEnum.GLOBAL));
            outValue.Add(createNewCategory(CategoryIdEnum.FOOD, CategoryIdEnum.GLOBAL));
            outValue.Add(createNewCategory(CategoryIdEnum.RENT, CategoryIdEnum.RENT));
            outValue.Add(createNewCategory(CategoryIdEnum.VEGETABLE, CategoryIdEnum.FOOD));
            outValue.Add(createNewCategory(CategoryIdEnum.MEAT, CategoryIdEnum.FOOD));

            return outValue;
        }

        static public TransactionCategory getRootCategory()
        {
            return createNewCategory(CategoryIdEnum.GLOBAL, CategoryIdEnum.GLOBAL);
        }

        static private TransactionCategory createNewCategory(CategoryIdEnum id, CategoryIdEnum parentId)
        {
            TransactionCategory category = new TransactionCategory();
            category.Id = (long)id;
            category.ParentId = (long)parentId;
            return category;
        }
    }
}
