﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AccountHelper.Classes
{
    public static class DateTimeUtils
    {
        const int DAY_PER_WEEK = 7;

        /// <summary>
        /// get the given previous day of the week
        /// </summary>
        /// <param name="day">day to look from</param>
        /// <param name="dayOfWeek">day of week to look for</param>
        /// <returns>DateTime of the previous day of week</returns>
        static public DateTime getPreviousDayOfWeekDay(DateTime day,DayOfWeek dayOfWeek)
        {
            if (dayOfWeek == day.DayOfWeek)
                return day;

            int numberOfDayToSubstract = (day.DayOfWeek - dayOfWeek + DAY_PER_WEEK) % DAY_PER_WEEK;
            return day.AddDays(-numberOfDayToSubstract);
        }

        /// <summary>
        /// get the given next day of the week
        /// </summary>
        /// <param name="day">day to look from</param>
        /// <param name="dayOfWeek">day of week to look for</param>
        /// <returns>DateTime of the next day of week</returns>
        static public DateTime getNextDayOfWeekDay(DateTime day, DayOfWeek dayOfWeek)
        {
            if (dayOfWeek == day.DayOfWeek)
                return day;

            int numberOfDayToAdd = (dayOfWeek - day.DayOfWeek + DAY_PER_WEEK) % DAY_PER_WEEK;
            return day.AddDays(numberOfDayToAdd);
        }

        /// <summary>
        /// get the first day of the month of the given day
        /// </summary>
        /// <param name="day">day to look from</param>
        /// <returns>DateTime of the first day of the month</returns>
        static public DateTime getFirstDayOfMonth(DateTime day)
        {
            return new DateTime(day.Year, day.Month, 1);
        }

        /// <summary>
        /// get the last day of the month of the given day
        /// </summary>
        /// <param name="day">day to look from</param>
        /// <returns>DateTime of the last day of the month</returns>
        static public DateTime getLastDayOfMonth(DateTime day)
        {
            return new DateTime(day.Year, day.Month, DateTime.DaysInMonth(day.Year, day.Month));
        }
    }
}
