﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AccountHelper.Classes;
using Moq;
using AccountHelper.Views;
using AccountHelperUnitTest.Factories;

namespace AccountHelperUnitTest
{
    [TestClass]
    public class AccountViewModelUnitTest
    {
        [TestMethod]
        public void isSaveChangesCalledOnRowEditEndingCommandTriggered()
        {
            var dataManager = new Mock<ITransactionDataManager>();
            dataManager.Setup(obj => obj.GetTransactions(new DateTime(), new DateTime())).Returns(TransactionFactory.getTransactions());

            var transactionRetriever = new Mock<ITransactionCategoryRetriever>();
            transactionRetriever.Setup(obj => obj.getTransactionCategories()).Returns(TransactionCategoryFactory.getTransactionCategoriesValidTree());
            transactionRetriever.Setup(obj => obj.getRootCategory()).Returns(TransactionCategoryFactory.getRootCategory());

            var userRetriever = new Mock<IUserRetriever>();
            userRetriever.Setup(obj => obj.getUsers()).Returns(UserFactory.GetUsers());

            AccountViewModelDependencies dependencies = new AccountViewModelDependencies(
                dataManager.Object, 
                transactionRetriever.Object, 
                userRetriever.Object);

            AccountViewModel transactionViewModel = new AccountViewModel(new DateTime(), new DateTime(), dependencies);
            transactionViewModel.RowEditEndingCommand.Execute(null);

            dataManager.Verify(m => m.SaveChanges());
        }
    }
}
