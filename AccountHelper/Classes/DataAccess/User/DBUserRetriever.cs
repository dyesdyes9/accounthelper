﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountHelper.Classes
{
    class DBUserRetriever : IUserRetriever
    {

        public List<User> getUsers()
        {
            AccountHelperEntities context = new AccountHelperEntities();
            return (from em in context.Users.OrderBy(c => c.Name)
                    select em).ToList();
        }
    }
}
