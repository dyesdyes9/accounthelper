﻿using AccountHelper;

namespace AccountHelperUnitTest.Factories
{
    static public class AccountFactory
    {
        static public Account getAccount()
        {
            TransactionCategoryTreeManager categoryManager = TransactionCategoryTreeManagerFactory.get3LayersTree();
            return new Account(TransactionFactory.getTransactions(), categoryManager);
        }
    }
}
