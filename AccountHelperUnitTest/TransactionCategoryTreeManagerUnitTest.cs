﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AccountHelper;
using AccountHelperUnitTest.Factories;

namespace AccountHelperUnitTest
{
    /// <summary>
    /// Summary description for TransactionCategoryTreeManagerUnitTest
    /// </summary>
    [TestClass]
    public class TransactionCategoryTreeManagerUnitTest
    {
        [TestMethod]
        public void IsSameCategoryAChild()
        {
            TransactionCategoryTreeManager categoryManager = TransactionCategoryTreeManagerFactory.get3LayersTree();

            Assert.AreEqual(false, categoryManager.IsNodeAChild((long)CategoryIdEnum.GLOBAL, (long)CategoryIdEnum.GLOBAL));
            Assert.AreEqual(false, categoryManager.IsNodeAChild((long)CategoryIdEnum.FOOD, (long)CategoryIdEnum.FOOD));
        }

        [TestMethod]
        public void IsAChild()
        {
            TransactionCategoryTreeManager categoryManager = TransactionCategoryTreeManagerFactory.get3LayersTree();

            Assert.AreEqual(true, categoryManager.IsNodeAChild((long)CategoryIdEnum.VEGETABLE, (long)CategoryIdEnum.FOOD));
            Assert.AreEqual(true, categoryManager.IsNodeAChild((long)CategoryIdEnum.VEGETABLE, (long)CategoryIdEnum.GLOBAL));
            Assert.AreEqual(true, categoryManager.IsNodeAChild((long)CategoryIdEnum.RENT, (long)CategoryIdEnum.GLOBAL));
        }

        [TestMethod]
        public void IsNotAChild()
        {
            TransactionCategoryTreeManager categoryManager = TransactionCategoryTreeManagerFactory.get3LayersTree();

            Assert.AreEqual(false, categoryManager.IsNodeAChild((long)CategoryIdEnum.VEGETABLE, (long)CategoryIdEnum.RENT));
            Assert.AreEqual(false, categoryManager.IsNodeAChild((long)CategoryIdEnum.VEGETABLE, (long)CategoryIdEnum.MEAT));
            Assert.AreEqual(false, categoryManager.IsNodeAChild((long)CategoryIdEnum.GLOBAL, (long)CategoryIdEnum.FOOD));
        }

        [TestMethod]
        public void validateValidTree()
        {
            TransactionCategoryTreeManager categoryManager = TransactionCategoryTreeManagerFactory.get3LayersTree();

            Assert.AreEqual(true, categoryManager.verifyTreeOrganization());
        }

        [TestMethod]
        public void validateInvalidTree()
        {
            TransactionCategoryTreeManager categoryManager = TransactionCategoryTreeManagerFactory.get3LayersInvalidTree();

            Assert.AreEqual(false, categoryManager.verifyTreeOrganization());
        }

        [TestMethod]
        public void categoryDisplayItems()
        {
            TransactionCategoryTreeManager categoryManager = TransactionCategoryTreeManagerFactory.get3LayersTree();

            Assert.IsTrue(categoryManager.CategoryDisplayItems.Count > 4);

            Assert.AreEqual(categoryManager.CategoryDisplayItems[0].CategoryId, (long)CategoryIdEnum.GLOBAL);
            Assert.AreEqual(categoryManager.CategoryDisplayItems[1].CategoryId, (long)CategoryIdEnum.FOOD);
            Assert.AreEqual(categoryManager.CategoryDisplayItems[4].CategoryId, (long)CategoryIdEnum.RENT);
        }
    }
}
