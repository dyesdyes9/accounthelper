﻿using AccountHelper;
using AccountHelper.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountHelperUnitTest.Factories
{
    static public class TransactionFactory
    {
        static public List<Transaction> getTransactions()
        {
            List<Transaction> transactionList = new List<Transaction>();

            transactionList.Add(createTransaction(42.68m, (long)CategoryIdEnum.GLOBAL, (long)TransactionUser.BOB));
            transactionList.Add(createTransaction(542.778m, (long)CategoryIdEnum.RENT, (long)TransactionUser.BOB));
            transactionList.Add(createTransaction(-65.96m, (long)CategoryIdEnum.FOOD, (long)TransactionUser.BOB));
            transactionList.Add(createTransaction(-7.04m, (long)CategoryIdEnum.RENT, (long)TransactionUser.BOB));
            transactionList.Add(createTransaction(50m, (long)CategoryIdEnum.VEGETABLE, (long)TransactionUser.JOHN));
            transactionList.Add(createTransaction(50m, (long)CategoryIdEnum.MEAT, (long)TransactionUser.BOB));
            transactionList.Add(createTransaction(50m, (long)CategoryIdEnum.VEGETABLE, (long)TransactionUser.JOHN));
            return transactionList;
        }

       static private Transaction createTransaction(decimal amount, long categoryId, long userId)
        {
            Transaction item = new Transaction();
            item.Amount = amount;
            item.CategoryId = categoryId;
            item.UserId = userId;

            TransactionCategory category = new TransactionCategory();
            category.Id = categoryId;
            item.TransactionCategory = category;

            User user = new User();
            user.Id = userId;
            item.TransactionUser = user;

            return item;
        }
    }
}
