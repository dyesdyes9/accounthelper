﻿using AccountHelper;
using AccountHelper.Classes;
using Moq;

namespace AccountHelperUnitTest.Factories
{
    static public class TransactionCategoryTreeManagerFactory
    {
        static public TransactionCategoryTreeManager get3LayersTree()
        {
            var transactionRetriever = new Mock<ITransactionCategoryRetriever>();
            transactionRetriever.Setup(obj => obj.getTransactionCategories()).Returns(TransactionCategoryFactory.getTransactionCategoriesValidTree());
            transactionRetriever.Setup(obj => obj.getRootCategory()).Returns(TransactionCategoryFactory.getRootCategory());

            TransactionCategoryTreeManager outValue = new TransactionCategoryTreeManager(transactionRetriever.Object);
            return outValue;
        }

        static public TransactionCategoryTreeManager get3LayersInvalidTree()
        {
            var transactionRetriever = new Mock<ITransactionCategoryRetriever>();
            transactionRetriever.Setup(obj => obj.getTransactionCategories()).Returns(TransactionCategoryFactory.getTransactionCategoriesInvalidTree());
            transactionRetriever.Setup(obj => obj.getRootCategory()).Returns(TransactionCategoryFactory.getRootCategory());

            TransactionCategoryTreeManager outValue = new TransactionCategoryTreeManager(transactionRetriever.Object);
            return outValue;
        }
    }
}
