﻿using AccountHelper.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountHelper.Classes
{
    class DBTransactionDataManager : ITransactionDataManager
    {
        private AccountHelperEntities m_dbContext;

        public DBTransactionDataManager()
        {
            m_dbContext = new AccountHelperEntities();
        }

        public List<Transaction> GetTransactions(DateTime start, DateTime end)
        {
            return (from em in m_dbContext.Transactions.Where(c => c.Date >= start && c.Date <= end).OrderBy(c => c.Date)
                    select em).ToList();
        }

        public void SaveChanges()
        {
            m_dbContext.SaveChanges();
        }
    }
}
