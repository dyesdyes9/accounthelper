﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountHelper
{
    using CategoryId = System.Int64;
    using UserId = System.Int64;

    public class Account
    {
        private List<Transaction> transactions;

        public List<Transaction> Transactions
        {
            get { return transactions; }
            set { transactions = value; }
        }
        public TransactionCategoryTreeManager categoryManager;

        public Account(List<Transaction> transactions, TransactionCategoryTreeManager categoryManager)
        {
            this.transactions = transactions;
            this.categoryManager = categoryManager;
        }

        public decimal getGlobalAmount()
        {
            decimal globalAmount = 0;
            foreach (Transaction transaction in transactions)
            {
                globalAmount += transaction.Amount;
            }
            return globalAmount;
        }

        public decimal getTotalAmountPerCategory(
            CategoryId categoryId,
            TransactionCategoryAdditionType additionType = TransactionCategoryAdditionType.CATEGORY_ONLY)
        {
            decimal totalAmount = 0;
            foreach (Transaction transaction in transactions)
            {
                if (isRightCategory(transaction, categoryId, additionType))
                {
                    totalAmount += transaction.Amount;
                }
            }
            return totalAmount;
        }

        public decimal getTotalAmountPerUser(UserId userId)
        {
            decimal totalAmount = 0;
            foreach (Transaction transaction in transactions)
            {
                if (transaction.TransactionUser.Id == userId)
                {
                    totalAmount += transaction.Amount;
                }
            }
            return totalAmount;
        }

        public decimal getTotalAmountPerUserPerCategory(
            UserId userId,
            CategoryId categoryId,
            TransactionCategoryAdditionType additionType = TransactionCategoryAdditionType.CATEGORY_ONLY)
        {
            decimal totalAmount = 0;
            foreach (Transaction transaction in transactions)
            {
                if (transaction.TransactionUser.Id == userId && isRightCategory(transaction, categoryId, additionType))
                {
                    totalAmount += transaction.Amount;
                }
            }
            return totalAmount;
        }

        private bool isRightCategory(Transaction transactionToCompare, CategoryId categoryIdWeLookFor, TransactionCategoryAdditionType additionType)
        {
            return transactionToCompare.TransactionCategory.Id == categoryIdWeLookFor
                    || (additionType == TransactionCategoryAdditionType.CATEGORY_AND_CHILDREN
                        && categoryManager.IsNodeAChild(transactionToCompare.TransactionCategory.Id, categoryIdWeLookFor));
        }
    }
}
