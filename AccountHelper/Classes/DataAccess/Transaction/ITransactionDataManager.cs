﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountHelper.Classes
{
    public interface ITransactionDataManager
    {
        List<AccountHelper.Transaction> GetTransactions(DateTime start, DateTime end);

        void SaveChanges();
    }
}
