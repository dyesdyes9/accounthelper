﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using AccountHelper.Views;
using AccountHelper.Classes;

namespace AccountHelper.Views
{
    public enum PeriodType
    {
        [Description("Weekly")]
        PERIODTYPE_WEEKLY,
        [Description("Fortnightly")]
        PERIODTYPE_FORTNIGHTLY,
        [Description("Monthly")]
        PERIODTYPE_MONTHLY,
    }

    class ChoosePeriodViewModel : BaseViewModel
    {
        private PeriodType m_selectedPeriodType;
        private DateTime m_startRange;
        private DateTime m_endRange;
        private DateTime m_selectedDate;
        private AccountViewModel m_transactionViewModel;

        public AccountViewModel TransactionViewModel
        {
            get { return m_transactionViewModel; }
            set { m_transactionViewModel = value; }
        }

        public DateTime SelectedDate
        {
            get { return m_selectedDate; }
            set {
                if (m_selectedDate != value)
                {
                    m_selectedDate = value;
                    updateRangeDate();
                }
            }
        }
        
        private DateTime StartRange
        {
            get { return m_startRange; }
            set { m_startRange = value; }
        }

        private DateTime EndRange
        {
            get { return m_endRange; }
            set { m_endRange = value; }
        }

        public PeriodType SelectedPeriodType
        {
            get { return m_selectedPeriodType; }
            set {
                if (m_selectedPeriodType != value)
                {
                    m_selectedPeriodType = value;
                    updateRangeDate();
                }
            }
        }
        
        public ChoosePeriodViewModel()
        {
            StartRange = DateTime.Now;
            EndRange = DateTime.Now;
            AccountViewModelDependencies dependencies = new AccountViewModelDependencies(
                new DBTransactionDataManager(),
                new DBTransactionCategoryRetriever(),
                new DBUserRetriever());
            TransactionViewModel = new AccountViewModel(StartRange, EndRange, dependencies);

            SelectedDate = DateTime.Now;
            SelectedPeriodType = PeriodType.PERIODTYPE_MONTHLY;
        }

        /// <summary>
        /// Will update StartRange and EndRange depending on the period type and the selected date
        /// </summary>
        private void updateRangeDate()
        {
            switch (SelectedPeriodType)
	        {
            case PeriodType.PERIODTYPE_WEEKLY:
                {
                    StartRange = DateTimeUtils.getPreviousDayOfWeekDay(SelectedDate,DayOfWeek.Monday);
                    EndRange = DateTimeUtils.getNextDayOfWeekDay(SelectedDate, DayOfWeek.Monday);
                } break;
            case PeriodType.PERIODTYPE_FORTNIGHTLY:
                {
                    StartRange = DateTimeUtils.getPreviousDayOfWeekDay(SelectedDate, DayOfWeek.Monday);
                    DateTime endCurrentWeek = DateTimeUtils.getNextDayOfWeekDay(SelectedDate, DayOfWeek.Monday);
                    EndRange = endCurrentWeek.AddDays(7);
                } break;
            case PeriodType.PERIODTYPE_MONTHLY:
                {
                    StartRange = DateTimeUtils.getFirstDayOfMonth(SelectedDate);
                    EndRange = DateTimeUtils.getLastDayOfMonth(SelectedDate);
                } break;
	        }
            TransactionViewModel.resetAccount(StartRange, EndRange);
        }
    }
}
