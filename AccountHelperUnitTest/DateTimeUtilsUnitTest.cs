﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AccountHelper.Classes;

namespace AccountHelperUnitTest
{
    [TestClass]
    public class DateTimeUtilsUnitTest
    {
        [TestMethod]
        public void getMondayStartOfTheWeekWithNormalDay()
        {
            DateTime date = new DateTime(2016, 2, 13);
            DateTime startWeekDateTest = new DateTime(2016, 2, 8);

            DateTime startWeekDay = DateTimeUtils.getPreviousDayOfWeekDay(date, DayOfWeek.Monday);

            Assert.AreEqual(startWeekDateTest, startWeekDay);
        }

        [TestMethod]
        public void getMondayStartOfTheWeekWithEarlyInTheMonthDay()
        {
            DateTime date = new DateTime(2016, 1, 2);
            DateTime startWeekDateTest = new DateTime(2015, 12, 28);

            DateTime startWeekDay = DateTimeUtils.getPreviousDayOfWeekDay(date, DayOfWeek.Monday);

            Assert.AreEqual(startWeekDateTest, startWeekDay);
        }

        [TestMethod]
        public void getMondayStartOfTheWeekWithEarlyInTheMonthDay2()
        {
            DateTime date = new DateTime(2016, 3, 3);
            DateTime startWeekDateTest = new DateTime(2016, 2, 29);

            DateTime startWeekDay = DateTimeUtils.getPreviousDayOfWeekDay(date, DayOfWeek.Monday);

            Assert.AreEqual(startWeekDateTest, startWeekDay);
        }

        [TestMethod]
        public void getMondayStartOfTheWeekWithMondayDay()
        {
            DateTime date = new DateTime(2016, 2, 8);
            DateTime startWeekDateTest = new DateTime(2016, 2, 8);

            DateTime startWeekDay = DateTimeUtils.getPreviousDayOfWeekDay(date, DayOfWeek.Monday);

            Assert.AreEqual(startWeekDateTest, startWeekDay);
        }

        [TestMethod]
        public void getFridayStartOfTheWeekWithNormalDay()
        {
            DateTime date = new DateTime(2016, 2, 11);
            DateTime startWeekDateTest = new DateTime(2016, 2, 5);

            DateTime startWeekDay = DateTimeUtils.getPreviousDayOfWeekDay(date,DayOfWeek.Friday);

            Assert.AreEqual(startWeekDateTest, startWeekDay);
        }

        [TestMethod]
        public void getSundayStartOfTheWeekWithNormalDay()
        {
            DateTime date = new DateTime(2016, 2, 11);
            DateTime startWeekDateTest = new DateTime(2016, 2, 7);

            DateTime startWeekDay = DateTimeUtils.getPreviousDayOfWeekDay(date, DayOfWeek.Sunday);

            Assert.AreEqual(startWeekDateTest, startWeekDay);
        }

        /////////////////////////////////////////////////////////////////////////////////

        [TestMethod]
        public void getStartOfMonth()
        {
            DateTime date = new DateTime(2016, 2, 11);
            DateTime StartMonthDate = new DateTime(2016, 2, 1);

            DateTime startMonth = DateTimeUtils.getFirstDayOfMonth(date);

            Assert.AreEqual(StartMonthDate, startMonth);
        }

        /////////////////////////////////////////////////////////////////////////////////

        [TestMethod]
        public void getNextSundayWithNormalDay()
        {
            DateTime date = new DateTime(2016, 2, 13);
            DateTime endWeekDayTest = new DateTime(2016, 2, 14);

            DateTime endWeekDay = DateTimeUtils.getNextDayOfWeekDay(date, DayOfWeek.Sunday);

            Assert.AreEqual(endWeekDayTest, endWeekDay);
        }

        [TestMethod]
        public void getNextSundayWithLateInTheMonthDay()
        {
            DateTime date = new DateTime(2016, 2, 29);
            DateTime endWeekDayTest = new DateTime(2016, 3, 6);

            DateTime endWeekDay = DateTimeUtils.getNextDayOfWeekDay(date, DayOfWeek.Sunday);

            Assert.AreEqual(endWeekDayTest, endWeekDay);
        }

        [TestMethod]
        public void getNextSundayWithEarlyInTheMonthDay2()
        {
            DateTime date = new DateTime(2015, 12, 28);
            DateTime endWeekDayTest = new DateTime(2016, 1, 3);

            DateTime endWeekDay = DateTimeUtils.getNextDayOfWeekDay(date, DayOfWeek.Sunday);

            Assert.AreEqual(endWeekDayTest, endWeekDay);
        }

        [TestMethod]
        public void getNextSundayWithSundayDay()
        {
            DateTime date = new DateTime(2016, 2, 14);
            DateTime endWeekDayTest = new DateTime(2016, 2, 14);

            DateTime endWeekDay = DateTimeUtils.getNextDayOfWeekDay(date, DayOfWeek.Sunday);

            Assert.AreEqual(endWeekDayTest, endWeekDay);
        }

        [TestMethod]
        public void getNextFridayWithNormalDay()
        {
            DateTime date = new DateTime(2016, 2, 14);
            DateTime endWeekDayTest = new DateTime(2016, 2, 19);

            DateTime endWeekDay = DateTimeUtils.getNextDayOfWeekDay(date, DayOfWeek.Friday);

            Assert.AreEqual(endWeekDayTest, endWeekDay);
        }

        [TestMethod]
        public void getNextTuesdayWithNormalDay()
        {
            DateTime date = new DateTime(2016, 2, 14);
            DateTime endWeekDayTest = new DateTime(2016, 2, 16);

            DateTime endWeekDay = DateTimeUtils.getNextDayOfWeekDay(date, DayOfWeek.Tuesday);

            Assert.AreEqual(endWeekDayTest, endWeekDay);
        }

        //////////////////////////////////////////////////////////////////

        [TestMethod]
        public void getEndOfMonth()
        {
            DateTime date = new DateTime(2016, 2, 14);
            DateTime endMonthDayTest = new DateTime(2016, 2, 29);

            DateTime endMonthDay = DateTimeUtils.getLastDayOfMonth(date);

            Assert.AreEqual(endMonthDayTest, endMonthDay);
        }
    }
}
