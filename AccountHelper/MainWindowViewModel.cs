﻿using AccountHelper.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace AccountHelper
{
    public class MainWindowViewModel : BaseViewModel
    {
        public BaseViewModel m_currentViewModel;
        private Stack<BaseViewModel> m_lastViewModel;

        public MainWindowViewModel()
        {
            const int STACK_MAX_CAPACITY = 10;
            m_lastViewModel = new Stack<BaseViewModel>(STACK_MAX_CAPACITY);
        }

        #region Commands

        private ICommand m_changePageCommand;
        private ICommand m_backCommand;
        private ICommand m_homeCommand;
        private ICommand m_transactionCommand;

        public ICommand TransactionCommand
        {
            get
            {
                if (m_transactionCommand == null)
                {
                    m_transactionCommand = new ViewCommand(
                        param => ChangeViewModelWithHistoric(
                            new ChoosePeriodViewModel()));
                }

                return m_transactionCommand;
            }
        }

        public ICommand ChangePageCommand
        {
            get
            {
                if (m_changePageCommand == null)
                {
                    m_changePageCommand = new ViewCommand(
                        param => ChangeViewModelWithHistoric((BaseViewModel)param));
                }

                return m_changePageCommand;
            }
        }

        public ICommand BackCommand
        {
            get
            {
                if (m_backCommand == null)
                {
                    m_backCommand = new ViewCommand(
                        param => ChangeViewModel(m_lastViewModel.Pop()),
                        param => m_lastViewModel.Count > 0);
                }

                return m_backCommand;
            }
        }

        public ICommand HomeCommand
        {
            get
            {
                if (m_homeCommand == null)
                {
                    m_homeCommand = new ViewCommand(
                        param => ChangeViewModelWithHistoric(new HomeViewModel()));
                }

                return m_homeCommand;
            }
        }

        #endregion // Commands

        public BaseViewModel CurrentViewModel
        {
            get
            {
                if (m_currentViewModel == null)
                {
                    m_currentViewModel = new HomeViewModel();
                }

                return m_currentViewModel;
            }
            set
            {
                if (m_currentViewModel != value)
                {
                    m_currentViewModel = value;
                    OnPropertyChanged("CurrentViewModel");
                }
            }
        }

        private void ChangeViewModelWithHistoric(BaseViewModel viewModel)
        {
            m_lastViewModel.Push(CurrentViewModel);
            ChangeViewModel(viewModel);
        }

        private void ChangeViewModel(BaseViewModel viewModel)
        {
            CurrentViewModel = viewModel;
        }
    }
}
