﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace AccountHelper
{
    /// <summary>
    /// A command whose sole purpose is to relay its functionality to other
    /// objects by invoking delegates.
    /// </summary>
    class ViewCommand : ICommand
    {
        readonly Action<object> m_methodToExecute;
        readonly Predicate<object> m_conditionToVerify;

        /// <summary>
        /// When the execute must always be executed.
        /// </summary>
        /// <param name="methodToExecute">The method that holds the execution logic.</param>
        public ViewCommand(Action<object> methodToExecute)
            : this(methodToExecute, null)
        {
        }

        /// <summary>
        /// When the execute must always be executed.
        /// </summary>
        /// <param name="methodToExecute">The method that holds the execution logic.</param>
        /// /// <param name="conditionToVerify">The method that holds the logic to know whether it should be executed or not.</param>
        public ViewCommand(Action<object> methodToExecute, Predicate<object> conditionToVerify)
        {
            if (methodToExecute == null)
                throw new ArgumentNullException("methodToExecute");

            m_methodToExecute = methodToExecute;
            m_conditionToVerify = conditionToVerify;
        }

        /// <see cref="ICommand"/>
        public bool CanExecute(object parameters)
        {
            return m_conditionToVerify == null ? true : m_conditionToVerify(parameters);
        }

        /// <see cref="ICommand"/>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /// <see cref="ICommand"/>
        public void Execute(object parameters)
        {
            m_methodToExecute(parameters);
        }

    }
}
