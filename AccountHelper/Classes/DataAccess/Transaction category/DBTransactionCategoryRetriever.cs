﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountHelper.Classes
{
    class DBTransactionCategoryRetriever : ITransactionCategoryRetriever
    {
        public List<TransactionCategory> getTransactionCategories()
        {
            AccountHelperEntities context = new AccountHelperEntities();
            return (from em in context.TransactionCategories.OrderBy(c => c.Id)
                    select em).ToList();
        }


        public TransactionCategory getRootCategory()
        {
            AccountHelperEntities context = new AccountHelperEntities();
            List<TransactionCategory> list = (from em in context.TransactionCategories.Where(c => c.Id == c.ParentId)
                                              select em).Take(1).ToList();
            return (list.Count > 0) ? list[0] : null;
        }
    }
}
