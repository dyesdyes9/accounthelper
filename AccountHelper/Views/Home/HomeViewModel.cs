﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace AccountHelper.Views
{
    class HomeViewModel : BaseViewModel
    {
        List<User> m_userList;

        public List<User> UserList
        {
            get { return m_userList; }
            set { m_userList = value; }
        }

        public HomeViewModel()
        {
            AccountHelperEntities context = new AccountHelperEntities();
            
            m_userList = (  from em in context.Users.OrderBy(c => c.Id)
                            select em).ToList();
        }
    }
}
