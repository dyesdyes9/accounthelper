﻿using AccountHelper;
using System.Collections.Generic;

namespace AccountHelperUnitTest.Factories
{
    static public class UserFactory
    {
        static public List<User> GetUsers()
        {
            List<User> outList = new List<User>();

            outList.Add(createUser(1, "Bob"));
            outList.Add(createUser(2, "Martin"));
            outList.Add(createUser(3, "John"));

            return outList;
        }

        static private User createUser(long id, string name)
        {
            User user = new User();
            user.Id = id;
            user.Name = name;
            return user;
        }
    }
}
