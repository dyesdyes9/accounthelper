﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountHelper.Classes
{
    public interface ITransactionCategoryRetriever
    {
        List<TransactionCategory> getTransactionCategories();

        TransactionCategory getRootCategory();
    }
}
