﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountHelper
{
    using categoryId = System.Int64;

    public class TransactionCategoryNode
    {
        private categoryId m_id;
        private string m_name;
        private TransactionCategoryNode m_parent;
        private Dictionary<categoryId, TransactionCategoryNode> m_children;

        #region getters/setters

        public categoryId Id
        {
            get { return m_id; }
        }

        public string Name
        {
            get { return m_name; }
            set { m_name = value; }
        }

        public TransactionCategoryNode Parent
        {
            get { return m_parent; }
        }

        public Dictionary<categoryId, TransactionCategoryNode> Children
        {
            get { return m_children; }
            set { m_children = value; }
        }

        #endregion

        public TransactionCategoryNode(TransactionCategory category)
        {
            m_id = category.Id;
            m_name = category.Name;
            m_children = new Dictionary<categoryId, TransactionCategoryNode>();
        }

        public void SetParent(TransactionCategoryNode parent)
        {
            m_parent = parent;
        }

        public void AddChild(TransactionCategoryNode child)
        {
            m_children.Add(child.Id, child);
        }

        public void RemoveChild(TransactionCategoryNode child)
        {
            m_children.Remove(child.Id);
        }
    }
}
