# Account Helper #

This is a small personal project to get familiar with C#, WPF, MVVM. I tried using TDD as much as possible.

This small software allows you to add the expenditures you have spents (transaction).
Then once entered, you can get some statistics from it like how much paid everybody, on what, when etc...

You can configure the categories you want, in a tree like hierarchy. You can also configure different users and specify for each expenditure, who spent it.

I created the UI mockups with balsamiq, have a look, it's really simple to use.