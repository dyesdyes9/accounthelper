﻿using AccountHelper.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountHelper
{
    using System.Diagnostics;
    using categoryId = System.Int64;

    public enum TransactionCategoryAdditionType
    {
        CATEGORY_ONLY,
        CATEGORY_AND_CHILDREN
    }

    public class CategoryDisplayItem
    {
        private categoryId m_categoryId;

        public categoryId CategoryId
        {
          get { return m_categoryId; }
        }
        private string m_name;

        public string Name
        {
          get { return m_name; }
        }

        public CategoryDisplayItem(categoryId id, string name)
        {
            m_categoryId = id;
            m_name = name;
        }
    }

    public class TransactionCategoryTreeManager
    {
        private TransactionCategoryNode m_root;
        private Dictionary<categoryId, TransactionCategoryNode> m_nodes;
        private ITransactionCategoryRetriever m_transactionCategoryRetriever;
        private List<CategoryDisplayItem> m_categoryDisplayItems;

        public List<CategoryDisplayItem> CategoryDisplayItems
        {
            get { return m_categoryDisplayItems; }
            set { m_categoryDisplayItems = value; }
        }

        public TransactionCategoryTreeManager(ITransactionCategoryRetriever categoryRetriever)
        {
            m_transactionCategoryRetriever = categoryRetriever;
            m_nodes = new Dictionary<categoryId, TransactionCategoryNode>();
            InitTree();
            CategoryDisplayItems = initCategoryDisplayItems();
        }

        public bool IsNodeAChild(categoryId categoryIdToCheck, categoryId parentIdCategory)
        {
            if (getNode(categoryIdToCheck).Parent == null)
                return false;

            if (categoryIdToCheck == getNode(categoryIdToCheck).Parent.Id)
                return false; // if it is its own parent

            if (getNode(categoryIdToCheck).Parent.Id == parentIdCategory)
            {
                return true;
            }

            return IsNodeAChild(getNode(categoryIdToCheck).Parent.Id, parentIdCategory);
        }

        public string getCategoryName(categoryId categoryId)
        {
            return m_nodes.ContainsKey(categoryId) ? getNode(categoryId).Name : null;
        }

        private void Add(TransactionCategory categoryToAdd)
        {
            if (m_nodes.ContainsKey(categoryToAdd.Id))
                return;

            TransactionCategoryNode newNode = new TransactionCategoryNode(categoryToAdd);
            m_nodes.Add(categoryToAdd.Id, newNode);

            newNode.SetParent(getNode(categoryToAdd.ParentId));
            getNode(categoryToAdd.ParentId).AddChild(newNode);
        }

        private void SetRoot(TransactionCategory rootCategory)
        {
            m_root = new TransactionCategoryNode(rootCategory);
            m_nodes.Add(rootCategory.Id, m_root);
        }

        private void InitTree()
        {
            SetRoot(m_transactionCategoryRetriever.getRootCategory());

            List<TransactionCategory> categoryList = m_transactionCategoryRetriever.getTransactionCategories();
            foreach (TransactionCategory category in categoryList)
            {
                Add(category);
            }
        }

        private TransactionCategoryNode getNode(categoryId category)
        {
            return m_nodes[category];
        }

        /// <summary>
        /// Go through the tree to validate the relationships between nodes
        /// </summary>
        /// <returns>Whether the tree is valid or not</returns>
        public bool verifyTreeOrganization()
        {
            foreach (KeyValuePair<categoryId, TransactionCategoryNode> node in m_nodes)
            {
                if (node.Key != m_root.Id && !IsNodeAChild(node.Key, m_root.Id))
                {
                    return false; 
                }
            }

            return true;
        }

        /// <summary>
        /// Will go from root and go through every child recursiverly to add it to the list
        /// </summary>
        private List<CategoryDisplayItem> initCategoryDisplayItems()
        {
            List<CategoryDisplayItem> itemList = new List<CategoryDisplayItem>();

            fillListWithChildren(m_root, ref itemList);

            return itemList;
        }

        /// <summary>
        /// Recursive method that go through the node children and add then to the reference itemList
        /// </summary>
        /// <param name="node">Node that will be added to the itemList and its children will be added too</param>
        /// <param name="itemList">List that is filled with the parsed node data</param>
        private void fillListWithChildren(TransactionCategoryNode node, ref List<CategoryDisplayItem> itemList)
        {
            itemList.Add(new CategoryDisplayItem(node.Id, node.Name));

            foreach (KeyValuePair<categoryId, TransactionCategoryNode> childPair in node.Children)
            {
                fillListWithChildren(childPair.Value, ref itemList);
            }
        }
    }
}
