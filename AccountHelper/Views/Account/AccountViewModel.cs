﻿using AccountHelper.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace AccountHelper.Views
{
    public class AccountViewModelDependencies
    {

        public AccountViewModelDependencies(ITransactionDataManager transactionDataManager,
                                            ITransactionCategoryRetriever categoryRetriever,
                                            IUserRetriever userRetriever)
        {
            m_transactionDataManager = transactionDataManager;
            m_categoryRetriever = categoryRetriever;
            m_userRetriever = userRetriever;
        }

        public ITransactionDataManager m_transactionDataManager;
        public ITransactionCategoryRetriever m_categoryRetriever;
        public IUserRetriever m_userRetriever;
    }

    public class AccountViewModel : BaseViewModel
    {
        private Account m_account;
        private List<User> m_users;
        private TransactionCategoryTreeManager m_treeManager;
        private ITransactionDataManager m_transactionDataManager;
        private ICommand m_rowEditEndingCommand;

        public ICommand RowEditEndingCommand
        {
            get
            {
                if (m_rowEditEndingCommand == null)
                {
                    m_rowEditEndingCommand = new ViewCommand(
                        param => m_transactionDataManager.SaveChanges());
                }

                return m_rowEditEndingCommand;
            }
        }

        public TransactionCategoryTreeManager TreeManager
        {
            get { return m_treeManager; }
            set { m_treeManager = value; }
        }

        public List<User> Users
        {
            get { return m_users; }
            set { m_users = value; }
        }

        public Account Account
        {
            get { return m_account; }
            set { m_account = value; }
        }

        public AccountViewModel(DateTime start, DateTime end, AccountViewModelDependencies dependencies)
        {
            TreeManager = new TransactionCategoryTreeManager(dependencies.m_categoryRetriever);
            Users = dependencies.m_userRetriever.getUsers();
            m_transactionDataManager = dependencies.m_transactionDataManager;

            resetAccount(start, end);
        }

        public void resetAccount(DateTime start, DateTime end)
        {
            Account = new Account(m_transactionDataManager.GetTransactions(start, end), TreeManager);
        }
    }
}
