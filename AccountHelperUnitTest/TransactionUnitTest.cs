﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AccountHelper;
using AccountHelperUnitTest.Factories;

namespace AccountHelperUnitTest
{
    [TestClass]
    public class TransactionUnitTest
    {
        [TestMethod]
        public void GetGlobalTotalAmountFromListOfTransactions()
        {
            Account account = AccountFactory.getAccount();

            Assert.AreEqual(662.458m, account.getGlobalAmount());
        }

        [TestMethod]
        public void GetTotalAmountPerCategoryFromListOfTransactions()
        {
            Account account = AccountFactory.getAccount();

            Assert.AreEqual(42.68m, account.getTotalAmountPerCategory((long)CategoryIdEnum.GLOBAL));
            Assert.AreEqual(535.738m, account.getTotalAmountPerCategory((long)CategoryIdEnum.RENT));
            Assert.AreEqual(-65.96m, account.getTotalAmountPerCategory((long)CategoryIdEnum.FOOD));
        }

        [TestMethod]
        public void getTotalAmountPerCategoryAndSubCategories()
        {
            Account account = AccountFactory.getAccount();

            Assert.AreEqual(100m, account.getTotalAmountPerCategory((long)CategoryIdEnum.VEGETABLE, TransactionCategoryAdditionType.CATEGORY_AND_CHILDREN));
            Assert.AreEqual(84.04m, account.getTotalAmountPerCategory((long)CategoryIdEnum.FOOD, TransactionCategoryAdditionType.CATEGORY_AND_CHILDREN));
            Assert.AreEqual(account.getGlobalAmount(), account.getTotalAmountPerCategory((long)CategoryIdEnum.GLOBAL, TransactionCategoryAdditionType.CATEGORY_AND_CHILDREN));
        }

        [TestMethod]
        public void getTotalAmountPerUser()
        {
            Account account = AccountFactory.getAccount();

            Assert.AreEqual(562.458m, account.getTotalAmountPerUser((long)TransactionUser.BOB));
            Assert.AreEqual(100m, account.getTotalAmountPerUser((long)TransactionUser.JOHN));
        }

        [TestMethod]
        public void getTotalAmountPerUserPerCategory()
        {
            Account account = AccountFactory.getAccount();

            Assert.AreEqual(-65.96m, account.getTotalAmountPerUserPerCategory((long)TransactionUser.BOB, (long)CategoryIdEnum.FOOD));
            Assert.AreEqual(100m, account.getTotalAmountPerUserPerCategory((long)TransactionUser.JOHN, (long)CategoryIdEnum.VEGETABLE));
        }

        [TestMethod]
        public void getTotalAmountPerUserPerCategoryAndSubCategories()
        {
            Account account = AccountFactory.getAccount();

            Assert.AreEqual(-15.96m, account.getTotalAmountPerUserPerCategory(
                (long)TransactionUser.BOB,
                (long)CategoryIdEnum.FOOD,
                TransactionCategoryAdditionType.CATEGORY_AND_CHILDREN));

            Assert.AreEqual(100m, account.getTotalAmountPerUserPerCategory(
                (long)TransactionUser.JOHN,
                (long)CategoryIdEnum.FOOD,
                TransactionCategoryAdditionType.CATEGORY_AND_CHILDREN));
        }
    }

    public enum TransactionUser
    {
        UNKNOWN,
        BOB,
        JOHN,
    }
}



